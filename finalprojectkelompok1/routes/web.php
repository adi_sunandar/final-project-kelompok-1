<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BankController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RekeningController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.register');
});


Route::middleware(['guest'])->group(function () {
    Route::get('/',[BankController::class, 'landingpage']);
    Route::POST('/checkRekening',[BankController::class, 'apiBank']);
});


//grouping
Route::group(['prefix' => '/dashboard', 'middleware' => ['auth']], function(){
    Route::get('/',[BankController::class, 'dashboardBank']);
    Route::POST('/checkRekening',[BankController::class, 'apiBank']);
    
   


    Route::get('/profile', [ProfileController::class, 'index']);
    Route::get('/bank',[BankController::class, 'index']);


    //crud Laporan
    Route::prefix('/laporan')->group(function(){
    Route::get('/', [RekeningController::class, 'index']);
    Route::get('/{rekening:id}/show', [RekeningController::class, 'show']);
        
    Route::get('/create', [RekeningController::class, 'create']);
    Route::post('/buatLaporan', [RekeningController::class, 'store']);
    });//end group laporan

    Route::prefix('/profile')->group(function(){
        Route::get('/edit', [ProfileController::class, 'edit']);
        Route::PATCH('/{user:id}/password', [ProfileController::class, 'updatePassword']);
        Route::get('/password', [ProfileController::class, 'gantiPassword']);
        Route::PATCH('/{profile:id}', [ProfileController::class, 'update']);
    });//end group profile
});



//crud Profile



Auth::routes();