<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\User;
use App\Models\Rekening;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RekeningController extends Controller
{

    public function index(Rekening $rekening)
    {
        return view('dashboard.laporan', [
            'all_laporan' => $rekening::all()
        ]);
    }


    public function create()
    {
        $bank = Bank::all();
        return view('dashboard.buatlaporan', ['bank' => $bank]);
    }

    public function store(Request $request)
    {
       $validate = $request->validate([
            'nomor_rekening' => 'required|min:6',
            'atasnama' => 'required',
            'bank' => 'required',
            'kronologi' => 'required',
            'kateogri' => 'required',
            'kontak' => 'required',
        ]);
        
        Rekening::create([
            'nomor' => $request->input('nomor_rekening'),
            'nama_pemilik_rekening' => $request->input('atasnama'),
            'kategori' => $request->input('kateogri'),
            'laporan' => $request->input('kronologi'),
            'no_telephone_terlapor' => $request->input('kontak'),
            'bank_id' =>  $request->input('bank'),
            'users_id' => Auth::id()
        ]);

        return response()->json("Sukses buat laporan");
    }

    public function show(Request $request, Rekening $rekening){
        return view('dashboard.detailLaporan', [
            'dataLaporan' => $rekening
        ]);
    }
    
}
