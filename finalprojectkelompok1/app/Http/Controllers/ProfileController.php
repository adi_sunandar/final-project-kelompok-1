<?php

namespace App\Http\Controllers;

use auth;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use RealRashid\SweetAlert\Facades\Alert;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class ProfileController extends Controller
{
    
    public function index()
    {
        
        $profile = User::where('id', auth::id())->first();
        return view('dashboard.tampilprofile', ['profile' => $profile]);
    }

    public function edit()
    {
        $profile = User::where('id', auth::id())->first();
        return view('dashboard.editProfile', ['profile' => $profile]);
    }
            
    public function update(Request $request, Profile $profile){
    
        if(auth::id() != $profile->users_id){
            return response()->json("Anda Tidak punya akses untuk mengedit");
        }
        $this->validate($request,[
            'nama' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
        ]);

        
        Profile::where('id', $profile->id)->update([
            'nama'=> $request->nama,
            'alamat' => $request->alamat,
            'bio' => $request->bio
        ]);



        return response()->json("Berhasil Update");
    }


    public function gantiPassword()
    {
        return view('dashboard.gantiPassword');
    }

    public function updatePassword(Request $request, User $user)
    {

        $validate = $request->validate([
            'password' => ['required', 'min:6'],
            'password_confirm' => ['required', 'min:6'],
        ]);

        if($request->password !== $request->password_confirm){
           Alert::error('Password Tidak Sama');
           return Redirect::back();
        }

        $passwordHash = Hash::make($request->password);

        User::where('id', auth::id())->update([
            'password' => $passwordHash
        ]);

        Alert::success('Berhasil Ganti Password');
        return Redirect::back();

    }
            
    
    
 
   
}
