<?php

namespace App\Http\Controllers;

use auth;
use App\Models\Bank;
use App\Models\Rekening;
use App\ApiChecking\ApiBank;
use Illuminate\Http\Request;


class BankController extends Controller
{
    public function index()
    {
        return view('dashboard.bank', [
            'all_bank' => Bank::all()
        ]);
    }

    public function dashboardBank(Bank $bank, Rekening $rekening)
    {
        //return jumlah bank
        $laporanTerbaru = Rekening::offset(0)->limit(5)->get();
        $countbank = count($bank::get()) + 1;
        $countLaporanUser = count(Rekening::where('users_id', auth::id())->get());
        $countLaporanAllUser = count(Rekening::all());
        return view('dashboard.index', [
            'bank' => $countbank,
            'laporanUser' => $countLaporanUser,
            'laporanAllUser' => $countLaporanAllUser,
            'laporanTerbaru' => $laporanTerbaru
            
        ]);
    }

    public function landingPage()
    {
        $countLaporanAllUser = count(Rekening::all());
        return view('landingpage', [

            'laporanUser' => $countLaporanAllUser,
            
        ]);
    }


    public function apiBank(Request $request)
    {
        $requestApi = new ApiBank;

        $checkLaporan = Rekening::where('nomor', $request->norek)->get();
            if(count($checkLaporan) > 0){
                return view('dashboard.tableCheckRekening', [
                    'checkLaporan' => $checkLaporan
                ]);
            }
        

        $json = \json_decode($requestApi->bankCheck($request->norek));
        if($json === "false"){
            return response()->json("Rekening tersebut belum pernah dilaporkan");
        }else{
            return view('dashboard.tablesApi', [
                'dataApi' => $json
            ]);
        }
    }
}
