<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $table = "profile";
    protected $guarded = ['id'];
    use HasFactory;

    // protected $fillable = ['nama', 'alamat', 'bio', 'users_id'];

    // public function userProfile()
    // {
    //     return $this->belongsTo(User::class);
    // }

}
