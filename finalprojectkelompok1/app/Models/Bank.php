<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = "bank";
    protected $guarded = ['id'];
    use HasFactory;


    public function rekening()
    {
        return $this->hasMany(Rekening::class);
    }
}
