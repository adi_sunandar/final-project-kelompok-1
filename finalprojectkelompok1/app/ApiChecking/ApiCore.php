<?php

namespace App\ApiChecking;

class ApiBank
{
    public function bankCheck($nomor)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
        CURLOPT_URL => "https://fecore.my.id/demo/Api/Request.php?nomor_rekening=$nomor",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        return $response;

    }
}