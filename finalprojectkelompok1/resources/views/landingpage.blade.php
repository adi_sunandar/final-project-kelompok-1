@extends('dashboard.layouts.master')

@push('scripts')
<script src="{{ asset('templates')}}/assets/vendor/simple-datatables/simple-datatables.js"></script>
@endpush
@push('styles')
  <link href="{{ asset('templates') }}/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="{{ asset('templates') }}/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{ asset  ('templates') }}/assets/vendor/simple-datatables/style.css" rel="stylesheet">
@endpush
@section('main')
<div class="pagetitle">
  {{-- <h1>Dashboard</h1> --}}
  <nav>
    <ol class="breadcrumb">
      {{-- <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item active">Dashboard</li> --}}
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">

    <!-- Left side columns -->
    <div class="col-lg-8">
      <div class="row">

        <!-- Customers Card -->
        <div class="col-xxl-4 col-xl-12">

          <div class="card info-card customers-card">

            <div class="card-body">
              <h5 class="card-title">Semua Laporan User</h5>

              <div class="d-flex align-items-center">
                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                  <i class="bi bi-people"></i>
                </div>
                <div class="ps-3">
                  <h6><span class="text-success small pt-1 fw-bold">{{ $laporanUser }}</span></h6>
                  <span class="text-muted small pt-2 ps-1">Rekening Yang Dilaporkan</span>
                </div>
              </div>

            </div>
          </div>

        </div><!-- End Customers Card -->

        <!-- Reports -->
        <div class="col-12">
          <div class="card">

        <div class="card-body">
          <div class="modal" tabindex="-1" role="dialog" id="resultApi">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Result Pencarian</h5>
                  <button type="button" id="modalClose" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div id="page-api">

                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <h5 class="card-title">Cek Nomor Rekening </h5>

          <!-- FORM CHECK -->
          <form action="" method="POST" class="row g-3" id="formCheck">
            @csrf
            <div class="col-md-12">
              <div class="form-floating">
                <input type="number" name="norek" class="form-control" id="floatingName" placeholder="Nomor Rekening">
                <label for="floatingName">Nomor Rekening</label>
              </div>
            </div>
            <div class="col-md-12">

            
            <!-- End FORM CHECK -->
          
          </div>
        </form>
        <div class="text-center">
          <button onclick="cekrekening()" name="submit" class="btn btn-primary">Check</button>
          {{-- <button type="reset" class="btn btn-secondary">Reset</button> --}}
        </div>
          

          </div>
        </div><!-- End Reports -->
    </div>
      </div>
    </div><!-- End Left side columns -->

    <!-- Right side columns -->
      <!-- News & Updates Traffic -->
      <!-- End News & Updates -->

    </div><!-- End Right side columns -->
  </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  function cekrekening(){
   $.ajax({
    url: `{{ url('/checkRekening') }}`,
    method: "post",
    data: $('#formCheck').serialize(),
    success: function(data){
      if(data == "false"){
        swal("Good job!", "You clicked the button!", "success");
      }else{
        $('#resultApi').modal('show');
        $('#page-api').html(data);
      
      }
    },
    error: function(data){
      console.log(data);
    }
   })
  }
  
</script>
@endsection
