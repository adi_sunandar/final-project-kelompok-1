<div class="card-body">
    <table class="table table-borderless datatable">
      <thead>
        <tr>
          <th scope="col">Nomor</th>
          <th scope="col">Tanggal Pelaporan</th>
          <th scope="col">Kategori</th>
          <th scope="col">Kronologi (Jika ada)</th>
        </tr>
      </thead>
      <tbody>
         @foreach($dataApi->data->laporanDetail as $index => $data)
        <tr>
          <th scope="row"><?= $index + 1; ?></th>
          <td>
            {{ \Carbon\Carbon::createFromTimestampMs($data->laporanDate, 'Asia/Jakarta')->format('Y-m-d\ H:i') }} 
          </td>
          <td>{{ $data->kategoriAduan->deskripsi }}</td>
          <td>
            {{ $data->chronology }}
        </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>