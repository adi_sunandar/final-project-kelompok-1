<div class="card-body">

    <table class="table table-borderless datatable">
      <thead>
        <tr>
          <th scope="col">Nomor</th>
          <th scope="col">Tanggal Pelaporan</th>
          <th scope="col">Kategori</th>
          <th scope="col">Kronologi (Jika ada)</th>
        </tr>
      </thead>
      <tbody>
         @foreach($checkLaporan as $index => $data)
        <tr>
          <th scope="row">{{ $index + 1 }}</th>
          <td>
            {{ $data->created_at }} 
          </td>
          <td>{{ $data->kategori }}</td>
          <td>
            {{ $data->laporan }}
        </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>