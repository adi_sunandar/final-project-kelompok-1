@extends('dashboard.layouts.master')


@section('main')
<div class="pagetitle">
  <h1>Ganti Password</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Profile</a></li>
      <li class="breadcrumb-item active">Ganti Password</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">

    <!-- Left side columns -->
    <div class="col-lg-8">
      <div class="row">

        <!-- Reports -->
        </div><!-- End Reports -->
    </div>
        <!-- End Reports -->

        <!-- Recent Sales -->
        <div class="col-10">
            <div class="modal fade" tabindex="-1" id="modalProfile">
                <div class="modal-dialog modal-xl">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Ganti Password</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <div id="page-profile">
    
                    </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                  </div>
                </div>
              </div>
            <div class="container-fluid">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Ganti Password</h5>
    
                  <!-- Floating Labels Form -->
                  <form class="row g-3" method="post" action="{{auth::id()}}/password">
                    @csrf
                    @method('PATCH')
                    <div class="col-md-6">
                      <div class="form-floating">
                        <input type="password" class="form-control" id="floatingEmail" name="password" placeholder="New Password">
                        <label for="floatingEmail">New Password</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-floating">
                        <input type="password" class="form-control" id="floatingEmail" name="password_confirm" placeholder="Password Verify">
                        <label for="floatingEmail">Password Verify</label>
                      </div>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-primary">Ganti Password</button>
                    </div>
                  </form>
                  <br>
                 
                  <!-- End floating Labels Form -->
    
                </div>
              </div>
        </div><!-- End Recent Sales -->
      </div>
    </div><!-- End Left side columns -->
  </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  function edit(id){
    $.ajax({
      url: `profile/edit`,
      method: "get",
      success: function(data){
        $('#modalProfile').modal('show');
        $('#page-profile').html(data);
        $('#simpan').click(function(){
            $.ajax({
                url: 'profile/'+id,
                method: "post",
                data: $('#formEdit').serialize(),
                success: function(data){
                  swal("Good job!", data + ". Page Otomatis akan terrefresh", "success");
                      setInterval(() => {
                        location.reload();
                    }, 3000);
                },
                error: function(data){
                  console.log(data);
                }
            });
        });
      }
    })
  }
  </script>
@endsection
@push('scripts')
<script src="{{ asset('templates')}}/js/demo/datatables-demo.js"></script>
<script src="{{ asset('templates')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
@endpush