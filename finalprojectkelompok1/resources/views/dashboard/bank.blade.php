@extends('dashboard.layouts.master')

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.js"></script>
<script>
    $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush
@section('main')
<div class="pagetitle">
  <h1>List Bank</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Bank</a></li>
      <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">

    <!-- Left side columns -->
    <div class="col-lg-8">
      <div class="row">

        <!-- Reports -->
        </div><!-- End Reports -->
    </div>
        <!-- End Reports -->

        <!-- Recent Sales -->
        <div class="col-10">

            <div class="container-fluid">

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">List Bank</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Bank</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($all_bank ?? '' as $key => $data)
                                    <tr>
                                        <td>{{ $key + 1}}</td>
                                        <td>{{ $data->nama}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- End Recent Sales -->
      </div>
    </div><!-- End Left side columns -->
  </div>
</section>
@endsection
@push('scripts')
<script src="{{ asset('templates')}}/js/demo/datatables-demo.js"></script>
<script src="{{ asset('templates')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
@endpush