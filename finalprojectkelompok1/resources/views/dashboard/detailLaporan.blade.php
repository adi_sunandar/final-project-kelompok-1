<form class="row g-3" method="POST" id="formLaporan">
    @csrf
    <div class="col-md-12">
      <div class="form-floating">
        <input type="text" class="form-control" name="nomor_rekening" id="floatingName" value="{{ $dataLaporan->nomor }}" readonly>
        <label for="floatingName">Nomor Rekening</label>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-floating">
        <input type="text" class="form-control" name="atasnama" id="floatingName" value="{{ $dataLaporan->nama_pemilik_rekening }}" readonly>
        <label for="floatingName">Nama Pemilik Rekening</label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-floating">
        <input type="text" class="form-control" name="atasnama" id="bank" value="{{$dataLaporan->bank->nama }}" readonly>
        <label for="bank">Bank</label>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-floating">
        <textarea name="kronologi" id="floatingLaporan" cols="100" rows="10" readonly>{{ $dataLaporan->laporan }}</textarea>
      </div>
    </div>
    {{-- <h6>Kategori</h6> --}}
    <div class="col-md-6">
      <div class="form-floating">
        <input type="text" class="form-control" name="kateogri" id="floatingName" value="{{ $dataLaporan->kategori }}" readonly>
        <label for="floatingName">Kategori, Contoh: Penipu Online</label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-floating">
        <input type="text" class="form-control" name="kontak" id="floatingName" value="{{ $dataLaporan->no_telephone_terlapor    }}" readonly>
        <label for="floatingName">Nomor Telpon Pelapor</label>
      </div>
    </div>
  
  </form>
  <br>