@extends('dashboard.layouts.master')


@section('main')
<div class="pagetitle">
  <h1>Profile</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item active">Profile</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">

    <!-- Left side columns -->
    <div class="col-lg-8">
      <div class="row">

        <!-- Reports -->
        </div><!-- End Reports -->
    </div>
        <!-- End Reports -->

        <!-- Recent Sales -->
        <div class="col-10">
            <div class="modal fade" tabindex="-1" id="modalProfile">
                <div class="modal-dialog modal-xl">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Edit Profile</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <div id="page-profile">
    
                    </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                  </div>
                </div>
              </div>
            <div class="container-fluid">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Profile</h5>
    
                  <!-- Floating Labels Form -->
                  <form class="row g-3">
                    <div class="col-md-12">
                      <div class="form-floating">
                        <input type="text" class="form-control" id="floatingName" value="{{ $profile->userProfile->nama }}" readonly>
                        <label for="floatingName">Your Name</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-floating">
                        <input type="text" class="form-control" id="floatingEmail" value="{{ $profile->userProfile->bio }}" readonly>
                        <label for="floatingEmail">Bio</label>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-floating">
                        <textarea class="form-control" placeholder="Address" id="floatingTextarea" style="height: 100px;" readonly>{{ $profile->userProfile->alamat }}</textarea>
                        <label for="floatingTextarea">Address</label>
                      </div>
                    </div>
                  </form>
                  <br>
                  <div class="text-center">
                    <button onclick="edit({{ $profile->userProfile->id }})" class="btn btn-warning">Edit</button>
                  </div>
                  <!-- End floating Labels Form -->
    
                </div>
              </div>
        </div><!-- End Recent Sales -->
      </div>
    </div><!-- End Left side columns -->
  </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  function edit(id){
    $.ajax({
      url: `profile/edit`,
      method: "get",
      success: function(data){
        $('#modalProfile').modal('show');
        $('#page-profile').html(data);
        $('#simpan').click(function(){
            $.ajax({
                url: 'profile/'+id,
                method: "post",
                data: $('#formEdit').serialize(),
                success: function(data){
                  swal("Good job!", data + ". Page Otomatis akan terrefresh", "success");
                      setInterval(() => {
                        location.reload();
                    }, 3000);
                },
                error: function(data){
                  console.log(data);
                }
            });
        });
      }
    })
  }


  function gantiPassword(id)
  {
    $.ajax({
      url: 'profile/password',
      method: "get",
      success: function(data){
        $('.modal-title').text("Ganti Passowrd");
        $('#modalProfile').modal('show');
        $('#page-profile').html(data);
        $('#simpanPassword').click(function(){
            $.ajax({
                url: 'profile/'+id+'/password,
                method: "post",
                data: $('#formPassword').serialize(),
                success: function(data){
                  // swal("Good job!", data + ". Page Otomatis akan terrefresh", "success");
                  //     setInterval(() => {
                  //       location.reload();
                  //   }, 3000);
                  $('#modalProfile').modal('show');
                  $('#page-profile').html(data);
                },
                error: function(data){
                  console.log(data);
                }
            });
        });
      }
    });
  }
  </script>
@endsection
@push('scripts')
<script src="{{ asset('templates')}}/js/demo/datatables-demo.js"></script>
<script src="{{ asset('templates')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
@endpush