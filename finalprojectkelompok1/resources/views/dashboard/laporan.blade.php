@extends('dashboard.layouts.master')


@section('main')
<div class="pagetitle">
  <h1>Laporan</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Laporan</a></li>
      <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">

    <!-- Left side columns -->
    <div class="col-lg-8">
      <div class="row">

        <!-- Reports -->
        </div><!-- End Reports -->
    </div>
        <!-- End Reports -->

        <!-- Recent Sales -->
        <div class="col-10">
            <div class="modal fade" tabindex="-1" id="modalLaporan">
                <div class="modal-dialog modal-xl">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Kronologi  Laporan</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <div id="page-laporan">
    
                    </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                  </div>
                </div>
              </div>
            <div class="container-fluid">

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    @auth
                        <div class="card-header py-3">
                            <button onclick="buatLaporan()" class="btn btn-success btn-sm">Buat Laporan</button>
                        </div>
                    @endauth
                    <div class="card-body py-4">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kategori</th>
                                        <th>Rekening</th>
                                        <th>Pemilik Rekening</th>
                                        <th>Tanggal Di Buat</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($all_laporan as $key  => $laporan)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $laporan->kategori }}</td>
                                        <td>{{ $laporan->nomor }}</td>
                                        <td>{{ $laporan->nama_pemilik_rekening }}</td>
                                        <td>{{ $laporan->created_at }}</td>
                                        <td>
                                            <button onclick="showLaporan({{ $laporan->id }})" class="btn btn-primary btn-sm">Informasi Lengkap</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- End Recent Sales -->
      </div>
    </div><!-- End Left side columns -->
  </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function buatLaporan()
    {
        $.ajax({
            url: `{{ url('dashboard/laporan/create') }}`,
            method: "GET",
            success: function(data){
                $('#modalLaporan').modal('show');
                $('#page-laporan').html(data);
                $('#submitLaporan').click(function(){
                    swal({
                    title: "Apakah data sudah benar?",
                    text: "Semua data yang anda masukkan tidak dapat dihapus dan diubah!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                        url: `{{ url('dashboard/laporan/buatLaporan') }}`,
                        method: "POST",
                        data: $('#formLaporan').serialize(),
                        success: function(data){
                            swal("Good job!", data + ". Page Otomatis akan terrefresh", "success");
                            setInterval(() => {
                                location.reload();
                            }, 3000);
                        },
                        error: function(data){
                            swal("Error!", "Pastikan semua data terisi.", "error");
                        }
                    });
                    } else {
                        swal("Oke. Cek lagi ya!");
                    }
                    });

                    
                });
            },
            error: function(data){
                swal("Error!", "Server Error.", "error");
            }
        })
    }


    function showLaporan(id){
        $.ajax({
            url: `laporan/`+id+`/show`,
            method: "get",
            success: function(data){
                $('#modalLaporan').modal('show');
                $('#page-laporan').html(data);
            },
            error: function(data){
                    console.log(data);
            }
        });
    }
    </script>
@endsection
@push('scripts')
<script src="{{ asset('templates')}}/js/demo/datatables-demo.js"></script>
<script src="{{ asset('templates')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
@endpush