<form class="row g-3" method="POST" id="formLaporan">
  @csrf
  <div class="col-md-12">
    <div class="form-floating">
      <input type="number" class="form-control" name="nomor_rekening" id="floatingName" placeholder="Nomor Rekening">
      <label for="floatingName">Nomor Rekening</label>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-floating">
      <input type="text" class="form-control" name="atasnama" id="floatingName" placeholder="Nama Pemilik Rekening">
      <label for="floatingName">Nama Pemilik Rekening</label>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-floating">
      <select name="bank" id="bank" class="form-control">
        <option value="" selected>Pilih Bank</option>
        @foreach ($bank as $data)
        <option value="{{ $data->id }}">{{ $data->nama }}</option>
        @endforeach
      </select>
      <label for="bank">Bank</label>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-floating">
      <textarea name="kronologi" id="floatingLaporan" cols="100" rows="10" placeholder="Jelaskan Kronologi secara lengkap"></textarea>
    </div>
  </div>
  {{-- <h6>Kategori</h6> --}}
  <div class="col-md-6">
    <div class="form-floating">
      <input type="text" class="form-control" name="kateogri" id="floatingName" placeholder="Kategori, Contoh: Penipu Online">
      <label for="floatingName">Kategori, Contoh: Penipu Online</label>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-floating">
      <input type="number" class="form-control" name="kontak" id="floatingName" placeholder="Nomor Rekening">
      <label for="floatingName">Nomor Telpon Pelapor</label>
    </div>
  </div>

</form>
<br>
<div class="text-center">
  <button id="submitLaporan" class="btn btn-primary">Submit</button>
  <button type="reset" class="btn btn-secondary">Reset</button>
</div>
