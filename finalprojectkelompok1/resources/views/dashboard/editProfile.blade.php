<form class="row g-3" method="post" id="formEdit">
    @csrf
    @method('PATCH')
    <div class="col-md-12">
      <div class="form-floating">
        <input type="text" class="form-control" id="floatingName" name="nama" value="{{ $profile->userProfile->nama }}">
        <label for="floatingName">Your Name</label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-floating">
        <input type="text" class="form-control" id="floatingEmail" name="bio" value="{{ $profile->userProfile->bio }}">
        <label for="floatingEmail">Bio</label>
      </div>
    </div>
    <div class="col-12">
      <div class="form-floating">
        <textarea class="form-control" name="alamat" placeholder="Address" id="floatingTextarea" style="height: 100px;">{{ $profile->userProfile->alamat }}</textarea>
        <label for="floatingTextarea">Address</label>
      </div>
    </div>
  </form>
  <br>
  <div class="text-center">
    <button id="simpan" class="btn btn-primary">Simpan</button>
  </div>