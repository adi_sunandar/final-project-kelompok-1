-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 02, 2022 at 09:34 PM
-- Server version: 8.0.30-0ubuntu0.22.04.1
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Bank Mandiri', NULL, NULL),
(2, 'Bank Mega', NULL, NULL),
(3, 'BCA - Bank Central Asia', NULL, NULL),
(4, 'BNI - Bank Negara Indonesia', NULL, NULL),
(5, 'Bank DKI', NULL, NULL),
(6, 'Bank Syariah Mandiri', NULL, NULL),
(7, 'BTN - Bank Tabungan Negara', NULL, NULL),
(8, 'Bank Artha Graha', NULL, NULL),
(9, 'Bank Permata', NULL, NULL),
(10, 'Bank Bukopin', NULL, NULL),
(11, 'Bank OCBC NISP', NULL, NULL),
(12, 'Bank Maybank Indonesia', NULL, NULL),
(13, 'Bank BPD', NULL, NULL),
(14, 'Bank Lainnya', NULL, NULL),
(15, 'Bank Jabar Banten', NULL, NULL),
(16, 'Bank Danamon', NULL, NULL),
(17, 'Bank CIMB Niaga', NULL, NULL),
(18, 'Bank BTPN', NULL, NULL),
(19, 'Bank UOB', NULL, NULL),
(20, 'Bank Muamalat', NULL, NULL),
(21, 'Bank BRI Syariah', NULL, NULL),
(22, 'Bank BNI Syariah', NULL, NULL),
(23, 'E-Wallet (Ovo  Gopay Dana  dll)', NULL, NULL),
(24, 'BSI - Bank Syariah Indonesia', NULL, NULL),
(25, 'Gopay BCA', NULL, NULL),
(26, 'OVO BCA', NULL, NULL),
(27, 'Doku', NULL, NULL),
(28, 'ShopeePay Mandiri', NULL, NULL),
(29, 'Dana BCA', NULL, NULL),
(30, 'Dana BNI', NULL, NULL),
(31, 'Dana BRI', NULL, NULL),
(32, 'Dana BTPN', NULL, NULL),
(33, 'Dana BTN', NULL, NULL),
(34, 'Dana Mandiri', NULL, NULL),
(35, 'Dana CIMB Niaga', NULL, NULL),
(36, 'Dana Bank Lain', NULL, NULL),
(37, 'Gopay BNI', NULL, NULL),
(38, 'Gopay BRI', NULL, NULL),
(39, 'Gopay BTN', NULL, NULL),
(40, 'Gopay CIMB Niaga', NULL, NULL),
(41, 'LinkAja BCA', NULL, NULL),
(42, 'LinkAja ATM Bersama', NULL, NULL),
(43, 'ShopeePay BCA', NULL, NULL),
(44, 'ShopeePay BNI', NULL, NULL),
(45, 'ShopeePay BRI', NULL, NULL),
(46, 'ShopeePay BSI', NULL, NULL),
(47, 'ShopeePay Permata', NULL, NULL),
(48, 'OVO Mandiri', NULL, NULL),
(49, 'OVO BNI', NULL, NULL),
(50, 'OVO CIMB Niaga', NULL, NULL),
(51, 'OVO Permata Bank', NULL, NULL),
(52, 'Dompetku', NULL, NULL),
(53, 'Bank Jago', NULL, NULL),
(54, 'Bank Sahabat Sampoerna', NULL, NULL),
(55, 'Bank NOBU', NULL, NULL),
(56, 'Bank Panin', NULL, NULL),
(57, 'bank Sinarmas', NULL, NULL),
(58, 'Bank MNC', NULL, NULL),
(59, 'Bank Mayapada', NULL, NULL),
(60, 'Bank Capital', NULL, NULL),
(61, 'Bank Raya', NULL, NULL),
(62, 'Bank Dana Syariah', NULL, NULL),
(63, 'SeaBank', NULL, NULL),
(64, 'Bank Jatim', NULL, NULL),
(65, 'Bank Aladin Syariah', NULL, NULL),
(66, 'Bank Neo Commerce', NULL, NULL),
(67, 'Bank KEB Hana', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
